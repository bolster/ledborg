#!/usr/bin/env python
# coding: Latin-1

# Load library functions we want
import time
import sys
import termios
import tty

# Function to get a keypress without showing text on screen
# this function is a bit more complex then the rest of the
# example, understanding it is a more advanced topic
def getkey():
    fn = sys.stdin.fileno()                                 # Get the file number used by standard input (the keyboard normally)
    oldAttr = termios.tcgetattr(fn)                         # Save the current settings for standard input behaviour
    try:                                                    # Attempt the following...
        tty.setraw(fn)                                          # Set the standard input file to raw mode (no buffering et cetera)
        key = sys.stdin.read(1)                                 # Read exactly 1 byte (character) from standard input
    finally:                                                # Regardless of wether we hit an exception (error) or not...
        termios.tcsetattr(fn, termios.TCSADRAIN, oldAttr)       # Set the settings for standard input behaviour back to what we saved earlier
    return key                                              # Return the character read from standard input

# Display the options to the user as a menu
print "+-------------------------------+"
print "|            LedBorg            |"
print "+-------------------------------+"
print "|                               |"
print "|    R      Cycle red           |"
print "|    G      Cycle green         |"
print "|    B      Cycle blue          |"
print "|    0      Set all off         |"
print "|    1      Set all 50%         |"
print "|    2      Set all 100%        |"
print "|    I      Invert all channels |"
print "|                               |"
print "|    Q      Quit                |"
print "|                               |"
print "+-------------------------------+"
print

# Loop indefinitely
while True:
    # Get the next user command
    command = getkey()                          # Read the next command from the user

    # Read the current colour from the LedBorg device
    LedBorg = open('/dev/ledborg', 'r')         # Open the LedBorg device for reading from
    oldColour = LedBorg.read()                  # Read the entire contents of the LedBorg file (should only be 3 characters long)
    LedBorg.close()                             # Close the LedBorg device
    red = int(oldColour[0])                     # Get the red setting from the colour string
    green = int(oldColour[1])                   # Get the green setting from the colour string
    blue = int(oldColour[2])                    # Get the blue setting from the colour string

    # Process the users command
    command = command.upper()                   # Convert the command to upper case (we do not care what case it is)
    if command == "R":                          # If the user pressed R...
        red += 1                                    # Increase red by 1
        if red > 2:                                 # If red is now above 100%
            red = 0                                     # Set red to off
    elif command == "G":                        # If the user pressed G...
        green += 1                                  # Increase green by 1
        if green > 2:                               # If green is now above 100%
            green = 0                                   # Set green to off
    elif command == "B":                        # If the user pressed B...
        blue += 1                                   # Increase blue by 1
        if blue > 2:                                # If blue is now above 100%
            blue = 0                                    # Set blue to off
    elif command == "0":                        # If the user pressed 0...
        red = 0                                     # Set red to off
        green = 0                                   # Set green to off
        blue = 0                                    # Set blue to off
    elif command == "1":                        # If the user pressed 1...
        red = 1                                     # Set red to 50%
        green = 1                                   # Set green to 50%
        blue = 1                                    # Set blue to 50%
    elif command == "2":                        # If the user pressed 2...
        red = 2                                     # Set red to 100%
        green = 2                                   # Set green to 100%
        blue = 2                                    # Set blue to 100%
    elif command == "I":                        # If the user pressed I...
        red = 2 - red                               # Set red to its inverse
        green = 2 - green                           # Set green to its inverse
        blue = 2 - blue                             # Set blue to its inverse
    elif command == "Q":                        # If the user pressed Q...
        break                                       # Jump out of the while loop
    elif command == "\3":                       # If the user pressed CTRL+C...
        break                                       # Jump out of the while loop
    else:                                       # If the user pressed anything else
        print "Unknown option '%s'" % (command)     # Print an error message
        continue                                    # Jump back to the start of the while loop (skip setting the colour)

    # Write the new colour to the LedBorg device
    newColour = "%d%d%d" % (red, green, blue)   # Create a text string of the form "RGB" from our new values
    LedBorg = open('/dev/ledborg', 'w')         # Open the LedBorg device for writing to
    LedBorg.write(newColour)                    # Write the new colour string to the LedBorg device
    LedBorg.close()                             # Close the LedBorg device
