#!/usr/bin/env python
# coding: Latin-1

# Load library functions we want
import time

# Setup our sqeuence list
colours = [
	"200", "210", "110", 
	"120", "020", "021", 
	"011", "012", "002", 
	"102", "101", "201"
	]

# Loop indefinitely
while True:
    for colour in colours:                  	# For each colour in the list in order...
        LedBorg = open('/dev/ledborg', 'w')     # Open the LedBorg device for writing to
        LedBorg.write(colour)                   # Write the colour string to the LedBorg device
        LedBorg.close()                         # Close the LedBorg device
        time.sleep(0.1)                           # Wait for 1 second 
