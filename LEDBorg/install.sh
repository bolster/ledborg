#!/bin/bash
# Set up an install directory and move to it
DEST="$HOME/src/ledborg"
mkdir -p $DEST
cd $DEST

# Grab the installation file and install everything
wget -O setup.zip http://www.piborg.org/downloads/ledborg/raspbian-2012-10-28-rev1.zip
unzip setup.zip
chmod +x install.sh
sudo ./install.sh