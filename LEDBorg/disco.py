#!/usr/bin/env python
# coding: Latin-1

# Load library functions we want
import time
import random

# Start the random number generator from a value, the default is the system timer
random.seed()

# Loop indefinitely
while True:
    red = random.randint(0,2)               # Generate a value for red between 0 and 2 inclusive
    green = random.randint(0,2)             # Generate a value for green between 0 and 2 inclusive
    blue = random.randint(0,2)              # Generate a value for blue between 0 and 2 inclusive
    colour = "%d%d%d" % (red, green, blue)  # Create a text string of the form "RGB" from our random values
    LedBorg = open('/dev/ledborg', 'w')     # Open the LedBorg device for writing to
    LedBorg.write(colour)                   # Write the colour string to the LedBorg device
    LedBorg.close()                         # Close the LedBorg device
    time.sleep(1)                           # Wait for 1 second 
